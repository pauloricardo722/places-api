Rails.application.routes.draw do
  mount_devise_token_auth_for 'Account', at: 'auth'

  resources :places, only: [:index, :create, :show] do
    resources :reviews, only: [:index, :create, :update]
  end

  get 'profile', to: 'accounts#current_account_info'
end
