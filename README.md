# PLACES API

* Ruby version
2.7.1

* Rails version
6.0.3.2

* **Configuration**



        docker-compose build

        docker-compose run app rails db:create

        docker-compose run app rails db:migrate

        docker-compose up -d

Create sample data
rails dev:setup

* **Services**
    Geocoder - https://github.com/alexreisner/geocoder

* **Heroku app**
https://paulobsa-places-api.herokuapp.com

* **Endpoints**

    post /auth - sign up
    ```
    {
        "email": "teste9@testemail.com",
        "password": "123456",
        "password_confirmation": "123456",
        "name": "teste9"
    }
    ```

    post /auth/sign_in (After sign-in use access-token, client and uid available in headers for authorization)
    ```
    {
        "email": "teste9@testemail.com",
        "password": "123456"
    }
    ```

    post /auth/sign_out

    post /places
    ```
    {
        "name": "Sport Club do Recife",
        "description": "Clube de Futebol",
        "street_name": "Rua Sport Club do Recife",
        "number": "SN",
        "city": "recife",
        "state": "pernambuco",
        "country": "brasil"
    }
    ```

    get /places
    query params available: sort=map, sort=list(default)

    get /places/:id

    post /places/:id/reviews
    ```
    {
        "rating": 4, # 1~5
        "comment": "Bom atendimento"
    }
    ```

    get /places/:id/reviews
