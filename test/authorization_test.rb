require 'test_helper'

class AuthorizationTest < ActionDispatch::IntegrationTest
  include AuthorizationHelper

  test 'sign up and log in user one' do
    user_one = sign_up_user_one
    assert_response :success

    auth_tokens_for_user(user_one)
    assert_response :success
  end

  test 'log in with wrong password' do
    sign_up_user_one
    assert_response :success

    auth_tokens_for_user({ email: 'userone@test.com', password: 'wrong_password'})
    assert_response :unauthorized
    assert_equal body, { 'success' => false, 'errors' => ['Invalid login credentials. Please try again.'] }.to_json
  end

  test 'sign up with an existent email' do
    sign_up_user_one
    assert_response :success

    sign_up_user_one
    assert_response :unprocessable_entity
    error_msg = JSON.parse(body)['errors']
    assert_equal error_msg, { 'email' => ['has already been taken'], 'full_messages' => ['Email has already been taken'] }
  end

  private

  def sign_up_user_one
    user_one = { email: 'userone@test.com', password: 'password', name: 'User test one' }
    sign_up(user_one)
    user_one
  end
end
