require 'test_helper'

class PlacesControllerTest < ActionDispatch::IntegrationTest
  include AuthorizationHelper

  setup do
    @place = places(:one)
    test_user = { email: 'user@test.com', password: 'testuser', name: 'test user' }
    sign_up(test_user)
    @auth_tokens = auth_tokens_for_user(test_user)
    @list_sort_response = file_fixture('places_list_sort.json').read
    @list_map_response = file_fixture('places_map_sort.json').read
    @test_coordinates = [-8.1676274,-34.9187994]

    Geocoder.configure(lookup: :test, ip_lookup: :test)
    Geocoder::Lookup::Test.set_default_stub(
      [
        {
          'coordinates' => @test_coordinates
        }
      ]
    )
  end

  test 'should get index' do
    path_params = { format: :json, sort: 'list' }
    get places_url(path_params), headers: @auth_tokens, as: :json

    assert_response :success
  end

  test "should get index with 'list' option" do
    path_params = { format: :json, sort: 'list' }
    get places_url(path_params), headers: @auth_tokens, as: :json

    assert_response :success
    assert_equal body, @list_sort_response
  end

  test "should get index with 'map' option" do
    path_params = { format: :json, sort: 'map' }
    get places_url(path_params), headers: @auth_tokens, as: :json
    assert_response :success
    assert_equal body, @list_map_response
  end

  test 'should create place' do
    assert_difference('Place.count') do
      post places_url,
           headers: @auth_tokens,
           params: {
             place: {
               description: "place description",
               name: "place name",
               street_name: "street one",
               number: 1,
               city: "recife",
               state: "pernambuco",
               country: "brasil",
               latitude: @test_coordinates.first,
               longitude: @test_coordinates.second
             }
           }, as: :json
    end

    assert_response :created
  end

  test 'should not create new place if name already taken' do
    post places_url,
         headers: @auth_tokens,
         params: {
           place: {
             description: @place.description,
             name: @place.name,
             street_name: "street one",
             number: 1,
             city: "recife",
             state: "pernambuco",
             country: "brasil",
             latitude: 0.0,
             longitude: 0.0
           }
         }, as: :json

    assert_response :unprocessable_entity
    assert_equal body, { 'name': ['has already been taken'] }.to_json
  end

  test 'should show place' do
    get place_url(@place), headers: @auth_tokens, as: :json
    assert_response :success
  end
end
