require 'test_helper'

class ReviewsControllerTest < ActionDispatch::IntegrationTest
  include AuthorizationHelper

  setup do
    @review = reviews(:one)
    @place = places(:one)

    test_user = { email: 'user@test.com', password: 'testuser', name: 'test user' }
    sign_up(test_user)
    @auth_tokens = auth_tokens_for_user(test_user)
  end

  test "should get index" do
    get place_reviews_url(@place), headers: @auth_tokens, as: :json
    assert_response :success
  end

  test "should create review" do
    assert_difference('Review.count') do
      post place_reviews_url(@place),
           headers: @auth_tokens,
           params: {
             place_id: @review.place_id,
             comment: @review.comment,
             rating: @review.rating,
             user_id: @review.account_id
           }, as: :json
    end

    assert_response 201
  end

  # test "should show review" do
  #   get place_review_url(@place.id, @review.id), as: :json
  #   assert_response :success
  # end

  test "should update review" do
    patch place_review_url(@place, @review), headers: @auth_tokens, params: { review: { comment: @review.comment, place_id: @review.place_id, rating: @review.rating, account_id: @review.account_id } }, as: :json
    assert_response 200
  end

  # test "should destroy review" do
  #   assert_difference('Review.count', -1) do
  #     delete place_review_url(@place, @review), headers: @auth_tokens, as: :json
  #   end

  #   assert_response 204
  # end
end
