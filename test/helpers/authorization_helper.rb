module AuthorizationHelper
  def sign_up(user)
    post '/auth/',
         params: {
           email: user[:email],
           password: user[:password],
           password_confirmation: user[:password],
           name: user[:name]
         }, as: :json
  end

  def auth_tokens_for_user(user)
    post '/auth/sign_in/',
         params: {
           email: user[:email],
           password: user[:password]
         }, as: :json
    # The three categories below are the ones you need as authentication headers.
    response.headers.slice('client', 'access-token', 'uid')
  end
end