require 'test_helper'

class ReviewTest < ActiveSupport::TestCase

  test 'rating must be exist' do
    assert_not reviews(:reviews_without_rating).valid?
  end

  test 'description must be exist' do
    assert_not reviews(:reviews_without_comment).valid?
  end

  test 'rating must be greater or equal to 1' do
    @review = reviews(:one)
    @review.rating = 0
    assert_not @review.valid?
    assert_equal @review.errors.messages, { rating: ['Rating value out of the range 1-5'] }
  end

  test 'rating must be less or equal to 5' do
    @review = reviews(:one)
    @review.rating = 6
    assert_not @review.valid?
    assert_equal @review.errors.messages, { rating: ['Rating value out of the range 1-5'] }
  end
end
