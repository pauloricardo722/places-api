require 'test_helper'

class PlaceTest < ActiveSupport::TestCase

  test 'should order by name asc' do
    assert_equal Place.all.order(name: :asc), Place.order_name_asc
  end

  test 'should order by proximity' do
    user_location = [-8.1676274, -34.9187994]
    expected_list_map = file_fixture('places_map_sort.json').read
    places = Place.order_by_proximity(user_location).as_json(except: [:created_at, :updated_at])

    assert_equal places.to_json, expected_list_map
  end
end
