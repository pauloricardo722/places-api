class ChangeAddressNumberType < ActiveRecord::Migration[6.0]
  def change
    change_column :places, :number, :string
  end
end
