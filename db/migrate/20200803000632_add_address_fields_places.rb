class AddAddressFieldsPlaces < ActiveRecord::Migration[6.0]
  def change
    add_column :places, :street_name, :string
    add_column :places, :number, :integer
    add_column :places, :city, :string
    add_column :places, :state, :string
    add_column :places, :country, :string
  end
end
