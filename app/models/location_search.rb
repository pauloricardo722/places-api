class LocationSearch
  def self.search_address_coordinates(term)
    result = Geocoder.search(term)
    { latitude: 0.0, longitude: 0.0 } if result.blank?

    lat = result.first.coordinates.first
    long = result.first.coordinates.second
    { latitude: lat, longitude: long }
  end

  def self.search_ip_coordinates(term)
    result = Geocoder.search(term)
    result.first.coordinates
  end

  def self.distance_between_coordinates(user_lat_long, location_lat_long)
    Geocoder::Calculations.distance_between(user_lat_long, location_lat_long)
  end
end