class Review < ApplicationRecord
  belongs_to :place
  belongs_to :account

  validates :rating, :comment, presence: true
  validates :rating, inclusion: { in: 1..5, message: 'Rating value out of the range 1-5' }

  def place_name
    place.name
  end

  def account_name
    account.name
  end
  
  def account_email
    account.email
  end

  def as_json(options={})
    super(
      root: false,
      methods: [:place_name, :account_name, :account_email],
      except: [:created_at, :updated_at]
    )
  end
end
