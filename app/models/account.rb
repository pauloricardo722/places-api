class Account < ActiveRecord::Base 
  extend Devise::Models
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :trackable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :reviews
  validates :name, presence: true

  include DeviseTokenAuth::Concerns::User
end
