class Place < ApplicationRecord
  has_many :reviews

  validates :name, uniqueness: true
  validates :name, :street_name, :number, :city, :state, :country, presence: true

  def self.order_name_asc
    Place.all.order(name: :asc)
  end

  def self.order_by_proximity(user_location)
    @place_distances = {}

    Place.all.each do |place|
      distance = LocationSearch.distance_between_coordinates(user_location, [place.latitude, place.longitude])
      @place_distances.store(distance, place)
    end

    @place_distances.sort.map(&:second)
  end
end
