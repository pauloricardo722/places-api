class PlacesController < BaseController
  before_action :set_place, only: [:show, :update, :destroy]
  before_action :render_sort_params_error, if: :invalid_sort_param?, only: [:index]
  before_action :get_request_location, only: [:index], if: :sort_by_map?

  LIST = 'list'.freeze
  MAP = 'map'.freeze

  def index
    @places = paginate Place.order_by_proximity(@user_location), per_page: per_page if sort_by_map?
    @places = paginate Place.order_name_asc, per_page: per_page if sort_by_list?

    render json: @places, except: [:updated_at, :created_at]
  end

  def show
    render json: @place
  end

  def create
    lat_long = search_lat_long
    @place = Place.new(place_params.merge(lat_long))

    if @place.save
      render json: @place, status: :created, location: @place
    else
      render json: @place.errors, status: :unprocessable_entity
    end
  end

  def update
    if @place.update(place_params)
      render json: @place
    else
      render json: @place.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @place.destroy
  end

  private

    def set_place
      @place = Place.find(params[:id])
    end

    def invalid_sort_param?
      [LIST, MAP].include?(sort_param) ? false : true
    end

    def sort_param
      sort_param = params.permit(:sort)
      sort_param = sort_param[:sort] || LIST
      sort_param.downcase
    end

    def render_sort_params_error
      render json: { error: "Invalid sort option. Only values 'list' or 'map' accepted" }, status: :unprocessable_entity
    end

    def render_location_error
      render json: { error: 'Error while trying to search user location' }, status: :unprocessable_entity
    end

    # Only allow a trusted parameter "white list" through.
    def place_params
      params.require(:place).permit(:name, :description, :street_name, :number, :city, :state, :country)
    end

    def per_page
      params[:per_page] || 20
    end

    def sort_by_list?
      sort_param == LIST
    end

    def sort_by_map?
      sort_param== MAP
    end

    def format_address
      params = place_params
      "#{params[:street_name]}, #{params[:number]}, #{params[:city]}, #{params[:state]}, #{params[:country]}"
    end

    def search_lat_long
      LocationSearch.search_address_coordinates(format_address)
    end

    def get_request_location
      @user_location = LocationSearch.search_ip_coordinates(request.remote_ip)
      render_location_error if @user_location.blank?

      @user_location
    end
end
