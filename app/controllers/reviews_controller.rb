class ReviewsController < BaseController
  before_action :set_review, only: [:show, :update, :destroy]

  def index
    @reviews = paginate Review.where(place_id: review_params[:place_id]).includes(:place, :account), per_page: per_page

    render json: @reviews
  end

  def show
    render json: @review
  end

  def create
    @review = Review.new(review_params)

    if @review.save
      render json: @review, status: :created, location: place_review_url(@review.place_id, @review.id)
    else
      render json: @review.errors, status: :unprocessable_entity
    end
  end

  def update
    if @review.update(review_params)
      render json: @review
    else
      render json: @review.errors, status: :unprocessable_entity
    end
  end

  # DELETE /reviews/1
  def destroy
    @review.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_review
      @review = Review.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def review_params
      params.permit(:place_id, :per_page, [:rating, :comment]).merge(account_id: current_account.id)
    end

    def per_page
      params[:per_page] || 20
    end
end
