class AccountsController < BaseController
  
  def current_account_info
    render json: { name: current_account.name, email: current_account.email }
  end
end
