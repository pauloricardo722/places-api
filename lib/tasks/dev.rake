namespace :dev do
  require 'faker'
  
  desc 'Create places and reviews for development tests'
  
  task setup: :environment do
    account = Account.find_by(name: 'teste1')
    account = Account.create(name: 'teste1', email: 'ok@ok.com', password: '123456') if account.nil?

    30.times do
      place = Place.create!(name: "#{Faker::Restaurant.name}-#{Time.now.to_i}", description: Faker::Restaurant.description, 
                            street_name: Faker::Address.street_name, number: rand(1..1000).to_s, city: Faker::Address.city, 
                            state: Faker::Address.state, country: Faker::Address.country, latitude: Faker::Address.latitude, 
                            longitude: Faker::Address.latitude)

      puts "place: #{place.name}"

      10.times do
        review = Review.create!(rating: rand(1..5), comment: Faker::Restaurant.review, place_id: place.id, account_id: account.id)
        puts "review: #{review.rating}"
      end
    end
  end
end
